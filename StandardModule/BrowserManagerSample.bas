Attribute VB_Name = "BrowserManagerSample"
Public Sub browserManagerOpenUrlSample()
    '定数定義
    Const BROWSER_URI As String = "ブラウザの絶対アドレス"
    Const SITE_URL As String = "開くサイトの絶対アドレス"

    '初期化
    Dim manager As New CBrowserManager
    manager.init BROWSER_URI
    
    '処理と戻り値出力
    Dim ret As Boolean
    ret = manager.openUrl(SITE_URL)
    If (ret) Then
        MsgBox "success"
    Else
        MsgBox "fail"
    End If
End Sub
