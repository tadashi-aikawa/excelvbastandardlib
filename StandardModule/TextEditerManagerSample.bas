Attribute VB_Name = "TextEditerManagerSample"
Public Sub TextEditerManagerOpenUriSample()
    '定数定義
    Const EDITER_URI As String = "エディターの絶対アドレス"
    Const FILE_URL As String = "開くファイルの絶対アドレス"

    '初期化
    Dim manager As New CTextEditerManager
    manager.init EDITER_URI
    
    '処理と戻り値出力
    Dim ret As Boolean
    ret = manager.openUri(FILE_URL)
    If (ret) Then
        MsgBox "success"
    Else
        MsgBox "fail"
    End If
End Sub

