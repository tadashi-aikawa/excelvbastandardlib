Attribute VB_Name = "FileManagerSample"
Public Sub fileManagerOpenUrlSample()
    '定数定義
    Const EXPLOLER_URI As String = "エクスプローラーの絶対アドレス"
    Const FILE_URI As String = "開くファイルの絶対アドレス"

    '初期化
    Dim manager As New CFileManager
    manager.init EXPLOLER_URI
    
    '処理と戻り値出力
    Dim ret As Boolean
    ret = manager.openUri(FILE_URI)
    If (ret) Then
        MsgBox "success"
    Else
        MsgBox "fail"
    End If
    
End Sub

Public Sub fileManagerCreateFolderSample()
    '定数定義
    Const FOLDER_URI As String = "作成するフォルダの絶対アドレス"

    '初期化
    Dim manager As New CFileManager
    Dim ret As CFileManager_Result
    
    '処理と戻り値出力
    ret = manager.createFolder(FOLDER_URI)
    Select Case ret
        Case CFileManager_Result.SUCCESS
            MsgBox "success"
        Case CFileManager_Result.FAIL
            MsgBox "fail"
        Case CFileManager_Result.ALREADY_EXISTED
            MsgBox "already existed"
    End Select
End Sub

Public Sub fileManagerCreateMakeFileSample()
    '定数定義
    Const FILE_URI As String = "作成するファイルの絶対アドレス"

    '初期化
    Dim manager As New CFileManager
    Dim ret As CFileManager_Result
    
    '処理と戻り値出力
    ret = manager.makeFile(FILE_URI, "tadashi", True)
    Select Case ret
        Case CFileManager_Result.SUCCESS
            MsgBox "success"
        Case CFileManager_Result.FAIL
            MsgBox "fail"
        Case CFileManager_Result.ALREADY_EXISTED
            MsgBox "already existed"
    End Select
End Sub

Public Sub fileManagerGetLastNameSample()
    '定数定義
    Const FILE_URI As String = "対象とするファイル/フォルダの絶対アドレス"

    '初期化
    Dim manager As New CFileManager
    
    '処理と戻り値出力
    Dim ret As String
    ret = manager.getLastName(FILE_URI)
    MsgBox ret
End Sub

Public Sub fileManagerCutLastNameSample()
    '定数定義
    Const FILE_URI As String = "対象とするファイル/フォルダの絶対アドレス"

    '初期化
    Dim manager As New CFileManager
    
    '処理と戻り値出力
    Dim ret As String
    ret = manager.cutLastName(FILE_URI)
    MsgBox ret
End Sub


