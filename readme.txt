*****************************************************************************************
*【最終更新日】2012/12/20
*****************************************************************************************
author: MAMAN

-----------------------------------------------------------------------------------------
 1. 概要
-----------------------------------------------------------------------------------------
本プロジェクトはExcelVBAで使用することができるクラス群です。
なお、本プロジェクトは開発中という位置づけであり、明確な動作保証はしておりません。
（開発環境は2007です）

本クラス群を利用して発生した如何なる事象に対しても保証はできません。
ご利用の際は自己責任でお願い致します。

使用可能はAPIにつきましては、品質が一定以上に達することを保証したのち作成する予定です。

-----------------------------------------------------------------------------------------
 2. ファイルの説明
-----------------------------------------------------------------------------------------

●ClassModule

 ・CBrowserManager.cls
   - ブラウザを操作することに特化したクラスです
   - 任意のURLをブラウザで開くことができます
   
 ・CFileManager.cls
   - ファイル/フォルダを操作することに特化したクラスです
   - ファイル作成/上書き、フォルダ作成、フォルダ/ファイルURIの抽出ができます
   
 ・CTextEditerManager.cls
   - テキストエディタを操作することに特化したクラスです
   - 任意のファイルをテキストエディタで開くことができます
     → （保証しませんがテキスト以外も可能かもしれません）
     
 ・CUriBuilder.cls
   - Uri構築に特化したクラスです
   - 他クラスを利用する際に必要です。「必ずインポートしてください」

●StandardModule

 ・BrowserManagerSample.bas
   - CBrowserManagerクラスの利用サンプルです
   
 ・FileManagerSample.bas
   - CFileManagerクラスの利用サンプルです
   
 ・TextEditerManagerSample.bas
   - CTextEditerManagerクラスの利用サンプルです
   
-----------------------------------------------------------------------------------------
 3. 使い方
-----------------------------------------------------------------------------------------
1. Microsoft Excelを立ち上げます
2. Microsoft Visual Basic Editerを立ち上げます
3. 導入したいプロジェクトにファイルをドラッグ＆ドロップします

以上

-----------------------------------------------------------------------------------------
 4. 他
-----------------------------------------------------------------------------------------