VERSION 1.1 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CUriBuilder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'URL
Private mUrl As String

'-------------------------------------------------
' 【概要】ベースURLを設定する
' 【引数】value: ベースURL
' 【戻値】なし
' 【備考】
'-------------------------------------------------
Public Sub path(ByVal value As String)
    mUrl = value
End Sub

'-------------------------------------------------
' 【概要】値を追加する
' 【引数】value: 値
' 【戻値】
' 【備考】
'-------------------------------------------------
Public Sub append(ByVal value As String)
    mUrl = mUrl + value
End Sub

'-------------------------------------------------
' 【概要】クエリパラメータを追加する
' 【引数】param: パラメータ名（キー名）
'         value: 値
' 【戻値】
' 【備考】
'-------------------------------------------------
Public Sub appendQueryParameter(ByVal param As String, ByVal value As String)
    mUrl = mUrl + "&" + param + "=" + value
End Sub

'-------------------------------------------------
' 【概要】ファイルパラメータを追加する
' 【引数】value: 値
' 【戻値】
' 【備考】
'-------------------------------------------------
Public Sub appendFileParameter(ByVal value As String)
    mUrl = mUrl + "\" + value
End Sub

'-------------------------------------------------
' 【概要】ファイル名を追加する
' 【引数】value: 名前
'         ext: 拡張子
' 【戻値】
' 【備考】
'-------------------------------------------------
Public Sub appendFile(ByVal value As String, Optional ext As String = "")
    mUrl = mUrl + "\" + value
    If (ext <> "") Then
        mUrl = mUrl + "." + ext
    End If
End Sub


'-------------------------------------------------
' プロパティ
'-------------------------------------------------
Public Property Get url() As String
    url = mUrl
End Property

