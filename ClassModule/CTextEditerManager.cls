VERSION 1.1 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CTextEditerManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit



'エディタURI
Private mEditerUri As String

'-------------------------------------------------
' 【概要】コンストラクタ
' 【備考】メンバの初期値を代入
'-------------------------------------------------
Public Sub Class_Initialize()
    '初期値としてメモ帳を代入
    mEditerUri = "NOTEPAD"
End Sub

'-------------------------------------------------
' 【概要】初期化
' 【引数】editerUri: エディタURI
' 【戻値】なし
' 【備考】
'-------------------------------------------------
Public Sub init(ByVal editerUri As String)
    mEditerUri = editerUri
End Sub

'-------------------------------------------------
' 【概要】指定したURIのテキストファイルを開く
' 【引数】uri: テキストファイルURI
' 【戻値】true: 成功 / false: 失敗
' 【備考】
'-------------------------------------------------
Public Function openUri(ByVal uri As String) As Boolean
    On Error GoTo openUri_Err
    
    Call Shell(mEditerUri & " " & uri, 1)
    openUri = True
    Exit Function
    
openUri_Err:
    Debug.Print "TextOpenError: CTextEditerManager# openUri"
    openUri = False
End Function

'-------------------------------------------------
' プロパティ
'-------------------------------------------------
Public Property Get editerUri() As String
    editerUri = mEditerUri
End Property



