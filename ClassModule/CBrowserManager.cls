VERSION 1.1 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CBrowserManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'ブラウザURI
Private mBrowserUri As String

'-------------------------------------------------
' 【概要】初期化
' 【引数】browserUri: ブラウザURI
' 【戻値】なし
' 【備考】
'-------------------------------------------------
Public Sub init(ByVal browserUri As String)
    mBrowserUri = browserUri
End Sub

'-------------------------------------------------
' 【概要】指定したURLを表示する
' 【引数】url: URL
' 【戻値】true:成功  false:失敗
' 【備考】initメソッドで初期化していない場合は必ずfalseを返します
'-------------------------------------------------
Public Function openUrl(ByVal url As String) As Boolean
    On Error GoTo openUrl_Err

    If (mBrowserUri <> "") Then
        Call Shell(mBrowserUri & " " & url, 1)
        openUrl = True
        Exit Function
    End If
    
    Debug.Print "ClassNotInitializedError: CBrowserManager# openUrl"
    openUrl = False
    Exit Function
    
openUrl_Err:
    Debug.Print "UrlOpenError: CBrowserManager# openUrl"
    openUrl = False
End Function

'-------------------------------------------------
' プロパティ
'-------------------------------------------------
Public Property Get browserUri() As String
    browserUri = mBrowserUri
End Property


