VERSION 1.1 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CFileManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'エクスプローラーURI
Private mExplolerUri As String

'結果
Public Enum CFileManager_Result
    SUCCESS
    ALREADY_EXISTED
    FAIL
End Enum



'-------------------------------------------------
' 【概要】初期化
' 【引数】explolerUri: エクスプローラーURI
' 【戻値】なし
' 【備考】
'-------------------------------------------------
Public Sub init(ByVal explolerUri As String)
    mExplolerUri = explolerUri
End Sub

'-------------------------------------------------
' 【概要】指定したURIをエクスプローラーで表示する
' 【引数】uri: URI
' 【戻値】true:成功  false:失敗
' 【備考】initメソッドで初期化していない場合は必ずfalseを返します
'-------------------------------------------------
Public Function openUri(ByVal uri As String) As Boolean
    On Error GoTo openUri_Err

    If (mExplolerUri <> "") Then
        Call Shell(mExplolerUri & " " & uri, 1)
        openUri = True
        Exit Function
    End If
    
    Debug.Print "ClassNotInitializedError: CFileManager# openUrl"
    openUri = False
    Exit Function
    
openUri_Err:
    Debug.Print "FileOpenError: CFileManager# openUri"
    openUri = False
End Function


'-------------------------------------------------
'【概要】フォルダを作成する
'【引数】uri: 作成するURI
'【戻値】結果(作成成功 / 既に存在する / 失敗)
'【備考】
'-------------------------------------------------
Public Function createFolder(ByVal uri As String) As CFileManager_Result
    Dim res As CFileManager_Result
    res = CFileManager_Result.FAIL

    Dim objFSO As Object
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    
    Dim divideUri As Variant
    divideUri = Split(uri, "\")
    
    Dim length As Long
    length = UBound(divideUri)
    
    Dim uriBuilder As CUriBuilder
    Set uriBuilder = New CUriBuilder
    
    '上位階層から順番に確認→最終的にフォルダが存在するようにする
    Dim cnt As Long
    For cnt = 0 To length
        If (cnt = 0) Then
            uriBuilder.path (divideUri(cnt))
        Else
            uriBuilder.appendFileParameter (divideUri(cnt))
        End If
        res = makeFolder(objFSO, uriBuilder.url)
    Next cnt
    
    Set objFSO = Nothing
    
    createFolder = res
    
    Select Case res
    Case FAIL
        Debug.Print "FolderNotCreatedError: CFileManager#createFolder"
    Case ALREADY_EXISTED
        Debug.Print "FolderAlwaysExistsWarning: CFileManager#createFolder"
    End Select
    
End Function

'-------------------------------------------------
'【概要】ファイルを作成する
'【引数】uri:   ファイルUri
'        data:  データの中身
'        canOverwrite: 上書きを許可するか (デフォルト false)
'【戻値】結果(作成成功 / 既に存在する / 失敗)
'【備考】上書きを許可したときは「既に存在する」になることはありません
'-------------------------------------------------
Public Function makeFile(ByVal uri, data As String, Optional canOverwrite As Boolean = False) As CFileManager_Result
    On Error GoTo Err
    Dim ret As CFileManager_Result
    
    Dim FSO As Object
    Set FSO = CreateObject("Scripting.FileSystemObject")
    If (FSO.FileExists(uri) = True And canOverwrite = False) Then
        ret = CFileManager_Result.ALREADY_EXISTED
        GoTo Finally
    End If
    
    With FSO.CreateTextFile(uri)
        .WriteLine data
        .Close
    End With
    Set FSO = Nothing
    
    ret = CFileManager_Result.SUCCESS
    GoTo Finally
Err:
    ret = CFileManager_Result.FAIL
    GoTo Finally
Finally:
    makeFile = ret
End Function

'-------------------------------------------------
'【概要】URIで指定したファイル/フォルダ名を返す
'【引数】uri: URI
'【戻値】ファイル/フォルダ名
'【備考】
'-------------------------------------------------
Public Function getLastName(ByVal uri As String) As String
    getLastName = Mid(uri, getLastYenPos(uri) + 1)
End Function

'-------------------------------------------------
'【概要】URIで指定したファイル/フォルダが存在するディレクトリを返す
'【引数】uri: URI
'【戻値】ファイル/フォルダ名
'【備考】
'-------------------------------------------------
Public Function cutLastName(ByVal uri As String) As String
    cutLastName = Left(uri, getLastYenPos(uri) - 1)
End Function



























'-------------------------------------------------
'【概要】フォルダを作成する
'【引数】objFSO: ファイルオブジェクト
'        uri: 作成するURI
'【戻値】Boolean: 成功(既存、新規作成）/失敗
'【備考】
'-------------------------------------------------
Private Function makeFolder(ByRef objFSO As Object, ByVal uri As String) As CFileManager_Result
    On Error GoTo ERROR
    
    If (objFSO.FolderExists(uri) = True) Then
        makeFolder = CFileManager_Result.ALREADY_EXISTED
        GoTo Finally
    End If
    
    'フォルダを生成する
    objFSO.createFolder uri
    makeFolder = CFileManager_Result.SUCCESS
    GoTo Finally
    
ERROR:
    makeFolder = CFileManager_Result.FAIL
    GoTo Finally
   
Finally:
        
End Function


'-------------------------------------------------
'【概要】文字列の最も後ろに存在する\のポジションを返す
'【引数】str: 文字列
'【戻値】\のポジション
'【備考】
'-------------------------------------------------
Private Function getLastYenPos(ByVal str As String)
    Dim n As Long
    Dim pos As Long
    Do
        n = InStr(n + 1, str, "\")
        If n > 0 Then
            pos = n
        Else
            Exit Do
        End If
    Loop
    getLastYenPos = pos
End Function

'-------------------------------------------------
' プロパティ
'-------------------------------------------------
Public Property Get explolerUri() As String
    explolerUri = mExplolerUri
End Property

